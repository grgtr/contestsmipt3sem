#include <algorithm>
#include <cmath>
#include <iostream>
#include <map>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

struct Node {
  std::map<short int, int>
      to;  // index = hash тк ребра не в символы а в пары противополжных
  // элементов, значение = номер по порядку в массиве в порядке
  // добавления
  bool term = false;
  bool term_r = false;
  int amount_of_terms_down = 0;
  int upper_vrtx = 0;
  int level = 0;
  int index_cin = 0;
  int index_cin_r = 0;
  Node() {}
};

// int newvert = 0;
class Trie {
 public:
  Trie() { nodes_.push_back(Node()); }

  void Add(const std::string& str, bool reversed, int index_cin) {
    int vrtx = 0;
    for (int i = 0; i < static_cast<int>(str.size()); ++i) {
      short int smb = str[i] - 'a' + 1;
      if (nodes_[vrtx].to.find(smb) ==
          nodes_[vrtx]
              .to.end()) {  // номер под которым была добавлена вершина не задан
        nodes_.push_back(Node());
        nodes_[vrtx].to[smb] = int(nodes_.size()) - 1;
      }
      ++nodes_[vrtx].amount_of_terms_down;
      nodes_[nodes_[vrtx].to[smb]].upper_vrtx = vrtx;
      vrtx = nodes_[vrtx].to[smb];
      nodes_[vrtx].level = i + 1;
    }
    if (reversed) {
      nodes_[vrtx].term_r = true;
      nodes_[vrtx].index_cin_r = index_cin;
    } else {
      nodes_[vrtx].term = true;
      nodes_[vrtx].index_cin = index_cin;
    }
    dict_[str] = vrtx;
    ++nodes_[vrtx].amount_of_terms_down;
  }

  bool Search(const std::string_view& str) {
    int vrtx = 0;
    for (int i = 0; i < static_cast<int>(str.size()); ++i) {
      short int smb = str[i] - 'a' + 1;
      if (nodes_[vrtx].to.find(smb) == nodes_[vrtx].to.end()) {
        return false;
      }
      vrtx = nodes_[vrtx].to[smb];
    }
    return nodes_[vrtx].term;
  }

 protected:
  std::unordered_map<std::string, int>
      dict_;  // слово и номер терминальной вершины
  std::vector<Node> nodes_;
};

bool IsPalindrome(const std::string_view& word) {
  for (size_t i = 0; i < word.size() / 2; ++i) {
    if (word[i] != word[word.size() - i - 1]) {
      return false;
    }
  }
  return true;
}

class Solver : public Trie {
 public:
  Solver() = default;
  std::string Solve() {
    int nn;
    std::cin >> nn;
    std::vector<std::string> words(nn);
    for (int i = 0; i < nn; ++i) {
      std::string str;
      std::cin >> str;
      words[i] = str;
      Add(str, false, i);
      std::reverse(str.begin(), str.end());
      Add(str, true, i);
    }
    std::unordered_set<std::string> ans;
    std::string res;
    int amount = 0;
    for (int i = 0; i < nn; ++i) {
      std::string word(words[i]);
      std::reverse(word.begin(), word.end());
      Helper(word, i, amount, res, true);
      word = words[i];
      Helper(word, i, amount, res, false);
    }
    return std::to_string(amount) + "\n" + res;
  }

 private:
  void Helper(const std::string& word, int ind, int& amount, std::string& res,
              bool iter) {
    int vrtx = dict_[word];
    while (vrtx != 0) {
      iter != 0 ?: vrtx = nodes_[vrtx].upper_vrtx;
      if (iter ? nodes_[vrtx].term : nodes_[vrtx].term_r) {
        int str_len = nodes_[vrtx].level;
        int index_cin =
            iter ? nodes_[vrtx].index_cin : nodes_[vrtx].index_cin_r;
        std::string_view mid(word.begin() + str_len, word.end());
        if ((ind != index_cin) && IsPalindrome(mid)) {
          amount++;
          res += iter ? (std::to_string(index_cin + 1) + " " +
                         std::to_string(ind + 1) + "\n")
                      : (std::to_string(ind + 1) + " " +
                         std::to_string(index_cin + 1) + "\n");
        }
      }
      !iter ?: vrtx = nodes_[vrtx].upper_vrtx;
    }
  }
};

int main() { std::cout << Solver().Solve(); }
