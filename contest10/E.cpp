#include <iostream>
#include <map>
#include <set>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;

const int kSimple = 10;

int Hash(int smb1, int smb2) { return smb1 * kSimple + smb2; }

struct Node {
  map<short int, int> to;
  bool term = false;
  int amount_of_terms_down = 0;
  int level = 0;
  Node() {}
};

class Trie {
 public:
  Trie() { nodes_.push_back(Node()); }

  void Add(const string& str) {
    int vrtx = 0;
    for (int i = 0; i < static_cast<int>(str.size()); ++i) {
      int smb_cur = str[i] - '0';
      int smb_back = str[str.size() - i - 1] - '0';
      int hash = Hash(smb_cur, smb_back);
      if (nodes_[vrtx].to.find(hash) == nodes_[vrtx].to.end()) {
        nodes_.push_back(Node());
        nodes_[vrtx].to[hash] = int(nodes_.size()) - 1;
      }
      ++nodes_[vrtx].amount_of_terms_down;
      vrtx = nodes_[vrtx].to[hash];
      nodes_[vrtx].level = i + 1;
    }
    nodes_[vrtx].term = true;
    ++nodes_[vrtx].amount_of_terms_down;
  }

  bool Search(const string& str) {
    int vrtx = 0;
    for (int i = 0; i < static_cast<int>(str.size()); ++i) {
      int smb_cur = str[i] - '0';
      int smb_back = str[str.size() - i - 1] - '0';
      int hash = Hash(smb_cur, smb_back);
      if (nodes_[vrtx].to.find(hash) == nodes_[vrtx].to.end()) {
        return false;
      }
      vrtx = nodes_[vrtx].to[hash];
    }
    return nodes_[vrtx].term;
  }

  void Check(int kk, vector<int>& res) {
    for (const auto& node : nodes_) {
      if (node.amount_of_terms_down >= kk) {
        ++res[node.level];
      }
    }
  }
  void DfsSearch(int maxlevel, int kk, vector<int>& res) {
    visited_.resize(nodes_.size(), false);
    Dfs(0, maxlevel, kk, res, 0);
  }

  void Dfs(int vrtx, int maxlevel, int kk, vector<int>& res, int cur_level) {
    visited_[vrtx] = true;
    if (cur_level > maxlevel) {
      return;
    }
    if (nodes_[vrtx].amount_of_terms_down >= kk) {
      ++res[cur_level];
    }
    for (auto elem : nodes_[vrtx].to) {
      if (!visited_[elem.second]) {
        Dfs(elem.second, maxlevel, kk, res, cur_level + 1);
      }
    }
  }

 private:
  vector<Node> nodes_;
  vector<bool> visited_;
};

void Faster() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);
}

int main() {
  Faster();
  int nn;
  int kk;
  cin >> nn >> kk;
  vector<string> snils(nn);
  int max_sz = 0;
  for (int i = 0; i < nn; ++i) {
    cin >> snils[i];
    max_sz = std::max(max_sz, static_cast<int>(snils[i].size()));
  }
  Trie trie;
  for (int i = 0; i < nn; ++i) {
    trie.Add(snils[i]);
  }

  int qq;
  cin >> qq;
  vector<int> levels;
  int ll;
  int max_level = 0;
  for (int i = 0; i < qq; ++i) {
    cin >> ll;
    max_level = std::max(max_level, ll);
    levels.push_back(ll);
  }
  vector<int> ans(max_level + 1, 0);
  trie.Check(kk, ans);
  for (int level : levels) {
    if (level > max_level) {
      cout << 0 << "\n";
    } else {
      cout << ans[level] << "\n";
    }
  }
  return 0;
}
