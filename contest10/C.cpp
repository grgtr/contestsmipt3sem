#include <iostream>
#include <limits>
#include <queue>
#include <string>
#include <vector>

const unsigned int kAlpha = 26;

struct Node {
  Node()
      : amount_of_terms_down(0),
        link(std::numeric_limits<unsigned int>::max()),
        dp(std::numeric_limits<unsigned int>::max()),
        compressed(std::numeric_limits<unsigned int>::max()) {
    to.resize(kAlpha + 1, std::numeric_limits<unsigned int>::max());
    go.resize(kAlpha + 1, std::numeric_limits<unsigned int>::max());
    term = false;
  }

  std::vector<unsigned int> to;
  std::vector<unsigned int> go;
  bool term;
  unsigned int amount_of_terms_down;
  unsigned int link;
  unsigned int dp;
  unsigned int compressed;
};

class Trie {
 public:
  Trie() {
    nodes_.push_back(Node());
    res_.emplace_back(0);
  }

  unsigned int Add(std::string_view str) {
    unsigned int vrtx = 0;
    for (unsigned int i = 0; i < str.size(); ++i) {
      int smb = str[i] - 'a' + 1;
      if (nodes_[vrtx].to[smb] == std::numeric_limits<unsigned int>::max()) {
        nodes_.push_back(Node());
        nodes_[vrtx].to[smb] = nodes_.size() - 1;
        res_.emplace_back();
        res_.back().push_back(i);
      }
      ++nodes_[vrtx].amount_of_terms_down;
      vrtx = nodes_[vrtx].to[smb];
    }
    nodes_[vrtx].term = true;
    ++nodes_[vrtx].amount_of_terms_down;
    return vrtx;
  }

  void AhoStart() {
    nodes_[0].dp = 0;
    nodes_[0].compressed = 0;
    for (unsigned int symbol = 1; symbol < kAlpha + 1; ++symbol) {
      nodes_[0].go[symbol] =
          (nodes_[0].to[symbol] == std::numeric_limits<unsigned int>::max())
              ? 0
              : nodes_[0].to[symbol];
    }
  }

  void AhoCorasick() {
    AhoStart();
    std::queue<unsigned int> queue;
    queue.push(0);
    unsigned int from;
    unsigned int to;
    while (!queue.empty()) {
      from = queue.front();
      queue.pop();
      for (unsigned int smb_last = 1; smb_last < kAlpha + 1; ++smb_last) {
        to = nodes_[from].to[smb_last];
        auto& vrtx_to = nodes_[to];
        if (to == std::numeric_limits<unsigned int>::max()) {
          continue;
        }
        vrtx_to.link = (from != 0) ? nodes_[nodes_[from].link].go[smb_last] : 0;
        auto& vrtx_link = nodes_[vrtx_to.link];
        for (unsigned int smb_new = 1; smb_new < kAlpha + 1; ++smb_new) {
          vrtx_to.go[smb_new] =
              (vrtx_to.to[smb_new] == std::numeric_limits<unsigned int>::max())
                  ? vrtx_link.go[smb_new]
                  : vrtx_to.to[smb_new];
        }
        vrtx_to.dp = (vrtx_to.term) ? vrtx_link.dp + 1 : vrtx_link.dp;
        vrtx_to.compressed =
            (vrtx_link.term) ? vrtx_to.link : vrtx_link.compressed;
        queue.push(to);
      }
    }
  }

 protected:
  std::vector<Node> nodes_;
  std::vector<std::vector<unsigned int>> res_;
};

class Solver : public Trie {
 public:
  Solver() = default;

  void Input(std::vector<unsigned int>& num_rtx_in_trie,
             std::vector<int>& sz_of_word, unsigned int number_of_words) {
    for (unsigned int i = 0; i < number_of_words; ++i) {
      std::string word;
      std::cin >> word;
      sz_of_word[i] = word.size();
      num_rtx_in_trie.push_back(Add(word) - 1);
    }
  }

  std::string Solve(const std::string& str, unsigned int number_of_words) {
    std::vector<unsigned int> num_rtx_in_trie;
    std::vector<int> sz_of_word(number_of_words);
    Input(num_rtx_in_trie, sz_of_word, number_of_words);
    AhoCorasick();

    unsigned int vrtx = 0;
    unsigned int cur_vrtx;
    for (unsigned int i = 0; i < str.size(); ++i) {
      int smb = str[i] - 'a' + 1;
      vrtx = nodes_[vrtx].go[smb];
      cur_vrtx = vrtx;

      while (cur_vrtx != 0) {
        if (nodes_[cur_vrtx].term) {
          res_[cur_vrtx].emplace_back(i);
        }
        cur_vrtx = nodes_[cur_vrtx].compressed;
      }
    }
    return GetAns(number_of_words, num_rtx_in_trie, sz_of_word);
  }

  std::string GetAns(unsigned int number_of_words,
                     const std::vector<unsigned int>& num_rtx_in_trie,
                     const std::vector<int>& sz_of_word) {
    std::string ans;
    for (unsigned int i = 0; i < number_of_words; ++i) {
      unsigned int size = res_[num_rtx_in_trie[i] + 1].size();  // размер ответа
      ans += std::to_string(size - 1) + " ";
      for (unsigned int j = 1; j < size; ++j) {
        ans += std::to_string(res_[num_rtx_in_trie[i] + 1][j] + 1 -
                              (sz_of_word[i] - 1)) +
               " ";
      }
      ans += '\n';
    }
    return ans;
  }
};

void Faster() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);
}

int main() {
  Faster();
  std::string str;
  std::cin >> str;
  unsigned int number_of_words;
  std::cin >> number_of_words;
  std::cout << Solver().Solve(str, number_of_words);
}
