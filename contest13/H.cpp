#include <algorithm>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <vector>

template <typename T>
inline std::ostream& operator<<(std::ostream& os, const std::vector<T>& arr) {
  for (size_t i = 0; i < std::size(arr); ++i) {
    os << arr[i] << " ";
  }
  return os;
}

template <typename T>
inline std::istream& operator>>(std::istream& in, std::vector<T>& arr) {
  for (size_t i = 0; i < std::size(arr); ++i) {
    in >> arr[i];
  }
  return in;
}

namespace Constants {
/*
  const char kPrecision = 9;    // NOLINT
  const double kEps15 = 1e-15;  // NOLINT
  const double kEps9 = 1e-9;    // NOLINT
*/
const int kFive = 5;
}  // namespace Constants

template <typename T>
struct Point {
  std::vector<T> coords;
  Point() = default;
  Point(const std::vector<T>& coords) : coords(coords) {}
  Point(const Point& other) : coords(other.coords) {}
  Point& operator=(const std::vector<T>& other) {
    coords = other;
    return *this;
  }
  Point& operator=(const Point& other) {
    coords = other.coords;
    return *this;
  }
  Point(std::initializer_list<T> coordinates) {
    for (auto coord : coordinates) {
      coords.push_back(coord);
    }
  }
  template <typename... Args>
  Point(Args... args) : coords{args...} {}
  ~Point() = default;

  size_t Measure() const { return coords.size(); }

  // T& operator[](size_t i) { return coords[i]; }
  // const T& operator[](size_t i) const { return coords[i]; }
  // убрано для большей наглядности
};

template <typename T>
bool operator==(const Point<T>& p1, const Point<T>& p2) {
  if (p1.Measure() != p2.Measure()) {
    throw std::invalid_argument("Coords have different measures");
  }
  /*
    for (size_t i = 0; i < p1.Measure(); ++i) {
      if (std::abs(p1.coords[i] - p2.coords[i]) >= Constants::kEps15) {
        return false;
      }
    }
  */
  return true;
}

template <typename T>
bool operator!=(const Point<T>& p1, const Point<T>& p2) {
  return !(p1 == p2);
}

template <typename T>
long double Distance(const Point<T>& p1, const Point<T>& p2) {
  long double result = 0;
  if (p1.Measure() != p2.Measure()) {
    throw std::invalid_argument("Coords have different measures");
  }
  for (size_t i = 0; i < p1.Measure(); ++i) {
    result += (p1.coords[i] - p2.coords[i]) * (p1.coords[i] - p2.coords[i]);
  }
  return std::sqrt(result);
}

template <typename T>
class Vector {
 private:
  bool transposed_ = false;  // то бишь столбец
 public:
  std::vector<T> coords;
  Vector() = default;
  Vector(const std::vector<T>& coords) : coords(coords) {}
  Vector(const Vector& other) : coords(other.coords) {}
  Vector(const Point<T>& p1, const Point<T>& p2) {
    if (p1.Measure() != p2.Measure()) {
      throw std::invalid_argument("Coords have different measures");
    }
    for (size_t i = 0; i < p1.Measure(); ++i) {
      coords.push_back(p2.coords[i] - p1.coords[i]);
    }
  }
  Vector& operator=(const std::vector<T>& other) {
    coords = other;
    return *this;
  }
  Vector& operator=(const Vector& other) {
    coords = other.coords;
    return *this;
  }
  Vector(std::initializer_list<T> coordinates) {
    for (auto coord : coordinates) {
      coords.push_back(coord);
    }
  }
  template <typename... Args>
  Vector(Args... args) : coords{args...} {}

  ~Vector() = default;

  static void Transpose(Vector<T>& vec) { vec.transposed_ = !vec.transposed_; }
  bool IsTransposed() const { return transposed_; }

  size_t Measure() const { return coords.size(); }

  // T& operator[](size_t i) { return coords[i]; }
  // const T& operator[](size_t i) const { return coords[i]; }
  // убрано для большей наглядности

  template <typename U>
  static U Length(const Vector<T>& vec) {
    U length = 0;
    for (const auto& elem : vec.coords) {
      length += elem * elem;
    }
    return std::sqrt(length);
  }

  Vector<T>& operator+=(const Vector<T>& other) {
    if (this->Measure() != other.Measure()) {
      throw std::invalid_argument("Coords have different measures");
    }
    for (size_t i = 0; i < this->Measure(); ++i) {
      this->coords[i] += other.coords[i];
    }
    return *this;
  }

  Vector<T>& operator-=(const Vector<T>& other) {
    if (this->Measure() != other.Measure()) {
      throw std::invalid_argument("Coords have different measures");
    }
    for (size_t i = 0; i < this->Measure(); ++i) {
      this->coords[i] -= other.coords[i];
    }
    return *this;
  }

  template <typename U>
  Vector<T>& operator*=(U lambda) {
    for (size_t i = 0; i < this->Measure(); ++i) {
      this->coords[i] *= lambda;
    }
    return *this;
  }

  template <typename U>
  Vector<T>& operator/=(U lambda) {
    for (size_t i = 0; i < this->Measure(); ++i) {
      this->coords[i] /= lambda;
    }
    return *this;
  }

  static long double ScalarProduct(const Vector<T>& v1, const Vector<T>& v2) {
    if (v1.Measure() != v2.Measure()) {
      throw std::invalid_argument("Coords have different measures");
    }
    long double result = 0;
    for (size_t i = 0; i < v1.Measure(); ++i) {
      result += v1.coords[i] * v2.coords[i];
    }
    return result;
  }

  Vector<T>& Normalize() {
    long double length = Length(*this);
    for (size_t i = 0; i < this->Measure(); ++i) {
      this->coords[i] /= length;
    }
    return *this;
  }

  Vector<T>& RotateOnAngle(double angle) {
    // по-часовой
    Vector<T> newv(std::cos(angle) * coords[0] - std::sin(angle) * coords[1],
                   std::sin(angle) * coords[0] + std::cos(angle) * coords[1]);
    /*coords[0] = std::cos(angle) * coords[0] - std::sin(angle) * coords[1];
    coords[1] = std::sin(angle) * coords[0] + std::cos(angle) * coords[1];
     */
    *this = newv;
    return *this;
  }
};

template <typename T>
Vector<T> operator+(const Vector<T>& v1, const Vector<T>& v2) {
  if (v1.Measure() != v2.Measure()) {
    throw std::invalid_argument("Coords have different measures");
  }
  Vector<T> result;
  for (size_t i = 0; i < v1.Measure(); ++i) {
    result.coords.push_back(v1.coords[i] + v2.coords[i]);
  }
  return result;
}

template <typename T>
Vector<T> operator-(const Vector<T>& v1, const Vector<T>& v2) {
  if (v1.Measure() != v2.Measure()) {
    throw std::invalid_argument("Coords have different measures");
  }
  Vector<T> result;
  for (size_t i = 0; i < v1.Measure(); ++i) {
    result.coords.push_back(v1.coords[i] - v2.coords[i]);
  }
  return result;
}

template <typename T, typename U>
Vector<T> operator*(U lambda, const Vector<T>& vec) {
  Vector<T> result;
  for (size_t i = 0; i < vec.Measure(); ++i) {
    result.coords.push_back(vec.coords[i] * lambda);
  }
  return result;
}

template <typename T>
Vector<T> operator*(const Vector<T>& vec, T lambda) {
  Vector<T> result;
  for (size_t i = 0; i < vec.Measure(); ++i) {
    result.coords.push_back(vec.coords[i] * lambda);
  }
  return result;
}

template <typename T>
Vector<T> operator/(const Vector<T>& vec, T lambda) {
  Vector<T> result;
  for (size_t i = 0; i < vec.Measure(); ++i) {
    result.coords.push_back(vec.coords[i] / lambda);
  }
  return result;
}

template <typename T>
Vector<T> PointToVector(const Point<T>& point) {
  Point<T> null_point(std::vector<T>(point.Measure(), 0));
  return Vector<T>(null_point, point);
}

template <typename T>
Point<T> VectorToPoint(const Vector<T>& vector) {
  return Point<T>(vector.coords);
}

template <typename T>
T PsevdoVec(const Vector<T>& v1, const Vector<T>& v2) {
  if (v1.Measure() != v2.Measure()) {
    throw std::invalid_argument("Coords have different measures");
  }
  if (v1.Measure() != 2) {
    throw std::invalid_argument("vector1 does not have 2 coords");
  }
  if (v2.Measure() != 2) {
    throw std::invalid_argument("vector2 does not have 2 coords");
  }
  if (v1.IsTransposed()) {
    throw std::invalid_argument("vector1 is row");
  }
  if (v2.IsTransposed()) {
    throw std::invalid_argument("vector2 is row");
  }
  return v1.coords[0] * v2.coords[1] - v1.coords[1] * v2.coords[0];
}

template <typename T>
long double AnglesBetweenVectors(const Vector<T>& v1, const Vector<T>& v2) {
  return std::atan2(PsevdoVec(v1, v2), Vector<T>::ScalarProduct(v1, v2));
}

template <typename T>
class Line {
 public:
  T aa;
  T bb;
  T cc;

  Line() = default;
  Line(T aa, T bb, T cc) : aa(aa), bb(bb), cc(cc) {}
  Line(const Point<T>& p1, const Point<T>& p2)
          : aa(p1.coords[1] - p2.coords[1]),
            bb(p2.coords[0] - p1.coords[0]),
            cc(p1.coords[0] * p2.coords[1] - p2.coords[0] * p1.coords[1]) {}
  Line(const Line<T>& line) : aa(line.aa), bb(line.bb), cc(line.cc) {}
  Line& operator=(const Line<T>& line) {
    aa = line.aa;
    bb = line.bb;
    cc = line.cc;
    return *this;
  }
  ~Line() = default;

  Vector<T> GetNormal() { return Vector<T>(aa, bb); }

  Vector<T> GetDirection() { return Vector<T>(bb, -aa); }
};

template <typename T>
std::pair<Point<T>, long double> Perpendicularize(const Point<T>& point,
                                                  const Line<T>& line) {
  if (point.Measure() != 2) {
    throw std::invalid_argument("line does not have 2 coords");
  }
  long double dist_to_line = static_cast<long double>(
          (-line.aa * point.coords[0] - line.bb * point.coords[1] - line.cc) /
          std::sqrt(line.aa * line.aa + line.bb * line.bb));
  Point<T> point_on_line =
          Point<T>(line.aa * static_cast<T>(dist_to_line) /
                   std::sqrt(line.aa * line.aa + line.bb * line.bb) +
                   point.coords[0],
                   line.bb * static_cast<T>(dist_to_line) /
                   std::sqrt(line.aa * line.aa + line.bb * line.bb) +
                   point.coords[1]);
  return std::make_pair(point_on_line, dist_to_line);
}

template <typename T>
class Segment {
 private:
  Point<T> p1_;
  Point<T> p2_;  // точки границы
  Line<T> line_;

 public:
  bool segment = true;  // true - отрезок false - луч с началом в точке p1_
  Segment() = default;
  Segment(const Point<T>& p1, const Point<T>& p2) : p1_(p1), p2_(p2) {
    line_ = Line<T>(p1, p2);
  }
  Segment(const Segment<T>& segment)
          : p1_(segment.p1_), p2_(segment.p2_), line_(segment.line_) {}
  Segment& operator=(const Segment<T>& segment) {
    p1_ = segment.p1_;
    p2_ = segment.p2_;
    line_ = segment.line_;
  }
  ~Segment() = default;

  Line<T> GetLine() const { return line_; }

  Point<T> GetP1() const { return p1_; }

  Point<T> GetP2() const { return p2_; }
};

template <typename T>
long double DistToSegment(const Point<T>& point, const Segment<T>& segment) {
  auto [point_on_line, dist] = Perpendicularize<T>(point, segment.GetLine());
  dist = std::abs(dist);
  if (segment.segment) {
    Vector<T> v1(point_on_line, segment.GetP1());
    Vector<T> v2(point_on_line, segment.GetP2());
    v1.Normalize();
    v2.Normalize();
    if (v1 != v2) {
      return dist;
    }
    long double dist1 = Distance(point, segment.GetP1());
    long double dist2 = Distance(point, segment.GetP2());
    return std::min(dist1, dist2);
  }
  Vector<T> v1(point_on_line, segment.GetP1());
  Vector<T> v2(segment.GetP1(), segment.GetP2());
  v1.Normalize();
  v2.Normalize();
  return (v1 != v2) ? dist : Distance(point, segment.GetP1());
}

template <typename T>
class ConvexHull {
 public:
  ConvexHull(std::vector<Point<T>>& points);
  const std::vector<Point<T>>& GetHull() const { return points_; }
  std::size_t Size() const { return points_.size(); }
  T GetSquare();

 private:
  std::vector<Point<T>> points_;
  static Point<T> FindBoundaryPoint(const std::vector<Point<T>>& points);
  void AddPoints(const std::vector<Point<T>>& points);
};

template <typename T>
Point<T> ConvexHull<T>::FindBoundaryPoint(const std::vector<Point<T>>& points) {
  auto point_front = points.front();
  for (const auto& point : points) {
    if (point_front.coords[0] > point.coords[0] ||
        (point_front.coords[0] == point.coords[0] &&
         point_front.coords[1] > point.coords[1])) {
      point_front = point;
    }
  }
  return point_front;
}

template <typename T>
ConvexHull<T>::ConvexHull(std::vector<Point<T>>& points) {
  auto point_start = FindBoundaryPoint(points);
  std::sort(points.begin(), points.end(),
            [&point_start](const Point<T>& ll, const Point<T>& rr) {
              Vector<T> v1(point_start, ll);
              Vector<T> v2(point_start, rr);
              long long cross_prod = PsevdoVec<T>(v1, v2);
              return (cross_prod == 0)
                     ? (Vector<T>::template Length<long long>(v1) <
                        Vector<T>::template Length<long long>(v2))
                     : (cross_prod > 0);
            });
  AddPoints(points);
  std::reverse(points_.begin() + 1, points_.end());
}

template <typename T>
void ConvexHull<T>::AddPoints(const std::vector<Point<T>>& points) {
  for (const auto& point : points) {
    for (; 2 <= points_.size();) {
      if (PsevdoVec<T>(Vector<T>(points_[points_.size() - 2],
                                 points_[points_.size() - 1]),
                       Vector<T>(points_[points_.size() - 1], point)) > 0) {
        break;
      }
      points_.pop_back();
    }
    points_.push_back(point);
  }
}

template <typename T>
T ConvexHull<T>::GetSquare() {
  long long ans = 0;
  Point<T> base(0, 0);
  int sz = static_cast<int>(points_.size());
  for (int i = 0; i < sz; ++i) {
    ans += PsevdoVec<T>(Vector<T>(base, points_[i]),
                        Vector<T>(base, points_[(i + 1) % sz]));
  }
  return std::abs(ans);
}

int main() {
  int num_of_points;
  std::cin >> num_of_points;
  long long xx;
  long long yy;
  std::vector<Point<long long>> points(num_of_points);
  for (auto& point : points) {
    std::cin >> xx >> yy;
    point = Point<long long>(xx, yy);
  }

  ConvexHull hull(points);
  std::cout << hull.Size() << '\n';
  for (const auto& point : hull.GetHull()) {
    std::cout << point.coords[0] << " " << point.coords[1] << '\n';
  }
  long long square = hull.GetSquare();
  std::cout << square / 2 << '.' << (square % 2) * Constants::kFive;
}
