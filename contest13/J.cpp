#include <algorithm>
#include <array>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <vector>

namespace Constants {
const int k1e9 = 1e6;
const int kThree = 3;
}  // namespace Constants

template <typename T>
struct Point {
  std::array<T, 2> coords;
  Point() = default;
  Point(const std::array<T, 2>& coords) : coords(coords) {}
  Point(const Point& other) : coords(other.coords) {}
  Point& operator=(const std::array<T, 2>& other) {
    coords = other;
    return *this;
  }
  Point& operator=(const Point& other) {
    coords = other.coords;
    return *this;
  }
  Point(std::initializer_list<T> coordinates) {
    for (auto coord : coordinates) {
      coords.push_back(coord);
    }
  }
  template <typename... Args>
  Point(Args... args) : coords{args...} {}
  ~Point() = default;

  size_t Measure() const { return coords.size(); }

  // T& operator[](size_t i) { return coords[i]; }
  // const T& operator[](size_t i) const { return coords[i]; }
  // убрано для большей наглядности
};

template <typename T>
long double Distance(const Point<T>& p1, const Point<T>& p2) {
  long double result = 0;
  if (p1.Measure() != p2.Measure()) {
    throw std::invalid_argument("Coords have different measures");
  }
  for (size_t i = 0; i < p1.Measure(); ++i) {
    result += (p1.coords[i] - p2.coords[i]) * (p1.coords[i] - p2.coords[i]);
  }
  return std::sqrt(result);
}

template <typename T>
class Vector {
 private:
  bool transposed_ = false;  // то бишь столбец
 public:
  std::array<T, 2> coords;
  Vector() = default;
  Vector(const std::array<T, 2>& coords) : coords(coords) {}
  Vector(const Vector& other) : coords(other.coords) {}
  Vector(const Point<T>& p1, const Point<T>& p2) {
    if (p1.Measure() != p2.Measure()) {
      throw std::invalid_argument("Coords have different measures");
    }
    for (size_t i = 0; i < p1.Measure(); ++i) {
      coords[i] = p2.coords[i] - p1.coords[i];
    }
  }
  Vector& operator=(const std::array<T, 2>& other) {
    coords = other;
    return *this;
  }
  Vector& operator=(const Vector& other) {
    coords = other.coords;
    return *this;
  }
  Vector(std::initializer_list<T> coordinates) {
    for (auto coord : coordinates) {
      coords.push_back(coord);
    }
  }
  template <typename... Args>
  Vector(Args... args) : coords{args...} {}

  ~Vector() = default;

  static void Transpose(Vector<T>& vec) { vec.transposed_ = !vec.transposed_; }
  bool IsTransposed() const { return transposed_; }

  size_t Measure() const { return coords.size(); }

  // T& operator[](size_t i) { return coords[i]; }
  // const T& operator[](size_t i) const { return coords[i]; }
  // убрано для большей наглядности

  template <typename U>
  static U Length(const Vector<T>& vec) {
    U length = 0;
    for (const auto& elem : vec.coords) {
      length += elem * elem;
    }
    return std::sqrt(length);
  }

  Vector<T>& operator+=(const Vector<T>& other) {
    if (this->Measure() != other.Measure()) {
      throw std::invalid_argument("Coords have different measures");
    }
    for (size_t i = 0; i < this->Measure(); ++i) {
      this->coords[i] += other.coords[i];
    }
    return *this;
  }

  Vector<T>& operator-=(const Vector<T>& other) {
    if (this->Measure() != other.Measure()) {
      throw std::invalid_argument("Coords have different measures");
    }
    for (size_t i = 0; i < this->Measure(); ++i) {
      this->coords[i] -= other.coords[i];
    }
    return *this;
  }

  template <typename U>
  Vector<T>& operator*=(U lambda) {
    for (size_t i = 0; i < this->Measure(); ++i) {
      this->coords[i] *= lambda;
    }
    return *this;
  }

  template <typename U>
  Vector<T>& operator/=(U lambda) {
    for (size_t i = 0; i < this->Measure(); ++i) {
      this->coords[i] /= lambda;
    }
    return *this;
  }

  static long double ScalarProduct(const Vector<T>& v1, const Vector<T>& v2) {
    if (v1.Measure() != v2.Measure()) {
      throw std::invalid_argument("Coords have different measures");
    }
    long double result = 0;
    for (size_t i = 0; i < v1.Measure(); ++i) {
      result += v1.coords[i] * v2.coords[i];
    }
    return result;
  }

  Vector<T>& Normalize() {
    long double length = Length(*this);
    for (size_t i = 0; i < this->Measure(); ++i) {
      this->coords[i] /= length;
    }
    return *this;
  }

  Vector<T>& RotateOnAngle(double angle) {
    // по-часовой
    Vector<T> newv(std::cos(angle) * coords[0] - std::sin(angle) * coords[1],
                   std::sin(angle) * coords[0] + std::cos(angle) * coords[1]);
    /*coords[0] = std::cos(angle) * coords[0] - std::sin(angle) * coords[1];
    coords[1] = std::sin(angle) * coords[0] + std::cos(angle) * coords[1];
     */
    *this = newv;
    return *this;
  }
};

template <typename T>
Vector<T> operator+(const Vector<T>& v1, const Vector<T>& v2) {
  if (v1.Measure() != v2.Measure()) {
    throw std::invalid_argument("Coords have different measures");
  }
  Vector<T> result;
  for (size_t i = 0; i < v1.Measure(); ++i) {
    result.coords.push_back(v1.coords[i] + v2.coords[i]);
  }
  return result;
}

template <typename T>
Vector<T> operator-(const Vector<T>& v1, const Vector<T>& v2) {
  if (v1.Measure() != v2.Measure()) {
    throw std::invalid_argument("Coords have different measures");
  }
  Vector<T> result;
  for (size_t i = 0; i < v1.Measure(); ++i) {
    result.coords.push_back(v1.coords[i] - v2.coords[i]);
  }
  return result;
}

template <typename T, typename U>
Vector<T> operator*(U lambda, const Vector<T>& vec) {
  Vector<T> result;
  for (size_t i = 0; i < vec.Measure(); ++i) {
    result.coords.push_back(vec.coords[i] * lambda);
  }
  return result;
}

template <typename T>
Vector<T> operator*(const Vector<T>& vec, T lambda) {
  Vector<T> result;
  for (size_t i = 0; i < vec.Measure(); ++i) {
    result.coords.push_back(vec.coords[i] * lambda);
  }
  return result;
}

template <typename T>
Vector<T> operator/(const Vector<T>& vec, T lambda) {
  Vector<T> result;
  for (size_t i = 0; i < vec.Measure(); ++i) {
    result.coords.push_back(vec.coords[i] / lambda);
  }
  return result;
}

template <typename T>
T PsevdoVec(const Vector<T>& v1, const Vector<T>& v2) {
  if (v1.Measure() != v2.Measure()) {
    throw std::invalid_argument("Coords have different measures");
  }
  if (v1.Measure() != 2) {
    throw std::invalid_argument("vector1 does not have 2 coords");
  }
  if (v2.Measure() != 2) {
    throw std::invalid_argument("vector2 does not have 2 coords");
  }
  if (v1.IsTransposed()) {
    throw std::invalid_argument("vector1 is row");
  }
  if (v2.IsTransposed()) {
    throw std::invalid_argument("vector2 is row");
  }
  return v1.coords[0] * v2.coords[1] - v1.coords[1] * v2.coords[0];
}

template <typename T>
std::vector<Point<T>> Sum(std::vector<Point<T>>& aa,
                          std::vector<Point<T>>& bb) {
  size_t nn = aa.size();
  size_t mm = bb.size();
  std::vector<Point<T>> res;
  long long minax = Constants::k1e9;
  long long minbx = Constants::k1e9;
  long long index_first = -1;
  long long index_second = -1;
  for (size_t i = 0; i < aa.size(); ++i) {
    if (aa[i].coords[0] < minax) {
      minax = aa[i].coords[0];
      index_first = static_cast<long long>(i);
    }
  }

  for (size_t i = 0; i < bb.size(); ++i) {
    if (bb[i].coords[0] < minbx) {
      minbx = bb[i].coords[0];
      index_second = static_cast<long long>(i);
    }
  }
  res.push_back(
      Point<T>(aa[index_first].coords[0] + bb[index_second].coords[0],
               aa[index_first].coords[1] + bb[index_second].coords[1]));
  long long l1 = index_first;
  long long l2 = index_second;
  bool bl1 = true;
  bool bl2 = true;
  while (bl1 || bl2) {
    Vector<T> vec1(aa[l1], aa[(l1 + 1) % nn]);
    Vector<T> vec2(bb[l2], bb[(l2 + 1) % mm]);
    if (bl2 && PsevdoVec(vec1, vec2) <= 0) {
      res.push_back(Point<T>(res.back().coords[0] + vec2.coords[0],
                             res.back().coords[1] + vec2.coords[1]));
      l2 = (l2 + 1) % mm;
      if (l2 == index_second) {
        bl2 = false;
      }
    } else {
      res.push_back(Point<T>(res.back().coords[0] + vec1.coords[0],
                             res.back().coords[1] + vec1.coords[1]));
      l1 = (l1 + 1) % nn;
      if (l1 == index_first) {
        bl1 = false;
      }
    }
  }
  res.pop_back();
  return res;
}

template <typename T>

long long Check(const std::vector<Point<T>>& polygon, Point<T> point) {
  long long ll = 0;
  long long rr = polygon.size() - 1;
  Vector vec1(polygon[0], polygon[1]);
  while (ll + 1 < rr) {
    long long mm = (ll + rr) >> 1;
    Vector vec2(polygon[0], polygon[mm]);
    Vector vec3(polygon[0], point);
    if ((PsevdoVec(vec1, vec2) > 0 && PsevdoVec(vec2, vec3) < 0) ||
        (PsevdoVec(vec1, vec2) < 0 && PsevdoVec(vec2, vec3) > 0)) {
      rr = mm;
    } else {
      ll = mm;
    }
  }
  return rr;
}

template <typename T>
bool InPolygon(Point<T> point, std::vector<Point<T>>& polygon) {
  long long count = 0;
  long long nn = polygon.size();
  bool bl = true;
  for (long long j = 0; j < nn; ++j) {
    Point<T> aa = polygon[j];
    Point<T> bb = polygon[(j + 1) % nn];
    if (aa.coords[1] > bb.coords[1]) {
      std::swap(aa, bb);
    }
    Vector<T> vec1(aa, point);
    Vector<T> vec2(aa, bb);
    long long orient = PsevdoVec(vec1, vec2);
    if ((PsevdoVec(vec1, vec2)) == 0 && aa.coords[1] <= point.coords[1] &&
        point.coords[1] <= bb.coords[1] &&
        std::min(aa.coords[0], bb.coords[0]) <= point.coords[0] &&
        point.coords[0] <= std::max(aa.coords[0], bb.coords[0])) {
      bl = false;
      break;
    }
    if (bb.coords[1] <= point.coords[1] || aa.coords[1] > point.coords[1]) {
      continue;
    }
    if (orient > 0) {
      count++;
    }
  }
  if (bl) {
    return (count % 2 == 1);
  }

  return true;
}

template <typename T>
std::vector<Point<T>> ReadPoints(long long nn) {
  std::vector<Point<T>> points(nn);
  T xx;
  T yy;
  for (long long i = 0; i < nn; ++i) {
    std::cin >> xx >> yy;
    points[i].coords[0] = xx;
    points[i].coords[1] = yy;
  }
  return points;
}

template <typename T>
std::vector<Point<T>> Read() {
  std::vector<Point<T>> res;
  long long nn;
  std::cin >> nn;
  std::vector<Point<T>> aa = ReadPoints<T>(nn);
  res = aa;
  std::cin >> nn;
  std::vector<Point<T>> bb = ReadPoints<T>(nn);
  res = Sum(res, bb);
  std::cin >> nn;
  std::vector<Point<T>> cc = ReadPoints<T>(nn);
  res = Sum(res, cc);
  return res;
}

template <typename T>
void IsCorrectMagnemaytic(const std::vector<Point<T>>& res) {
  T xx;
  T yy;
  long long qq;
  std::cin >> qq;
  for (long long i = 0; i < qq; ++i) {
    Point<T> mm;
    std::cin >> xx >> yy;
    mm.coords[0] = xx * Constants::kThree;
    mm.coords[1] = yy * Constants::kThree;
    long long ind = Check(res, mm);
    std::vector<Point<T>> triangle(Constants::kThree);
    triangle[0] = res[0];
    triangle[1] = res[ind];
    triangle[2] = res[ind - 1];
    (InPolygon(mm, triangle)) ? std::cout << "YES\n" : std::cout << "NO\n";
  }
}

int main() {
  std::vector<Point<long long>> res = Read<long long>();

  Point<long long> right(-Constants::k1e9, -Constants::k1e9);
  for (auto& re : res) {
    if (re.coords[0] > right.coords[0]) {
      right = re;
    }
  }

  IsCorrectMagnemaytic<long long>(res);
}
