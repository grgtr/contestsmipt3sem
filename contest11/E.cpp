#include <algorithm>
#include <iostream>
#include <list>
#include <vector>

const int kSize = 1048576;
const int kAlphabetSz = 26;
const std::list<char> kAlphabet = {
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
        'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
};

void StableSort(const std::vector<int>& arr, std::vector<int>& cnt) {
  for (int i = 0; i < kSize; i++) {
    cnt[i] = 0;
  }
  int sz = static_cast<int>(arr.size());
  for (int i = 0; i < sz; i++) {
    ++cnt[arr[i]];
  }
  for (int i = 1; i < kSize; i++) {
    cnt[i] += cnt[i - 1];
  }
}

std::vector<int> StringToVector(const std::string_view& ss) {
  std::vector<int> arr(ss.size());
  for (size_t i = 0; i < ss.size(); i++) {
    arr[i] = ss[i];
  }
  return arr;
}

std::vector<int> BuildSuffixArray(const std::string_view& ss) {
  int sz = static_cast<int>(ss.size());
  std::vector<int> classes(sz, -1);
  std::vector<int> cnt(kSize + sz);
  StableSort(StringToVector(ss), cnt);
  std::vector<int> pp(sz);
  for (int i = sz - 1; i >= 0; i--) {
    pp[--cnt[ss[i]]] = i;
  }
  classes[pp[0]] = 0;
  for (int i = 1; i < sz; i++) {
    classes[pp[i]] = classes[pp[i - 1]];
    if (ss[pp[i]] != ss[pp[i - 1]]) {
      ++classes[pp[i]];
    }
  }
  int kk = 0;
  while (sz - (1 << kk) > 0) {
    std::vector<int> new_pp(sz);
    std::vector<int> new_classes(sz);
    new_pp[0] = (sz + pp[0] - (1 << kk)) % sz;
    for (int i = 1; i < sz; i++) {
      new_pp[i] = (sz + pp[i] - (1 << kk)) % sz;
    }
    StableSort(classes, cnt);
    for (int i = sz - 1; i >= 0; i--) {
      pp[--cnt[classes[new_pp[i]]]] = new_pp[i];
    }  // pp - искомая сортировка для 2^(kk+1)
    new_classes[pp[0]] = 0;
    for (int i = 1; i < sz; i++) {
      new_classes[pp[i]] = new_classes[pp[i - 1]];
      if (classes[pp[i]] != classes[pp[i - 1]] ||
          classes[(pp[i] + (1 << kk)) % sz] !=
          classes[(pp[i - 1] + (1 << kk)) % sz]) {
        ++new_classes[pp[i]];
      }
    }
    classes = new_classes;
    kk++;
  }
  return pp;
}

class BWT {
 public:
  BWT() = default;
  BWT(const std::vector<int>& suffix_array) : suffix_array_(suffix_array){};
  std::string GetBWT(const std::string_view& source,
                     const std::vector<int>& suffix_array);
  std::string ReverseBWT(const std::string_view& bwt, int pos_in_table);

 private:
  std::string bwt_;
  std::vector<int> suffix_array_;
  std::string source_;
};

std::string BWT::GetBWT(const std::string_view& source,
                        const std::vector<int>& suffix_array) {
  if (!bwt_.empty()) {
    return bwt_;
  }
  source_ = source;
  std::string ans;
  for (size_t i = 0; i < source.size(); i++) {
    ans += source[(suffix_array[i] + source.size() - 1) % source.size()];
  }
  bwt_ = ans;
  return ans;
}
std::string BWT::ReverseBWT(const std::string_view& bwt, int pos_in_table) {
  if (!source_.empty()) {
    return source_;
  }
  bwt_ = bwt;
  std::vector<int> count(kAlphabetSz, 0);
  std::vector<int> table(bwt.size());
  for (char symbol : bwt) {
    ++count[symbol - 'a'];
  }

  int sum = 0;
  for (auto& cnt : count) {
    sum += cnt;
    cnt = sum - cnt;
  }

  for (int i = 0; i < static_cast<int>(bwt.size()); ++i) {
    table[count[bwt[i] - 'a']++] = i;
  }

  int reverse_pos = table[pos_in_table];
  std::string ans;
  for (int i = 0; i < static_cast<int>(bwt.size()); ++i) {
    ans += bwt[reverse_pos];
    reverse_pos = table[reverse_pos];
  }
  source_ = ans;
  return ans;
}

class MTF {
 public:
  MTF() = default;
  std::string GetMTF(const std::string_view& source);
  std::string DecodeMTF(const std::string_view& mtf);

 private:
  std::string mtf_;
  std::string source_;
};
std::string MTF::GetMTF(const std::string_view& source) {
  if (!mtf_.empty()) {
    return mtf_;
  }
  source_ = source;
  std::list<char> alphabet = kAlphabet;
  mtf_.resize(source.size());
  for (int i = 0; i < static_cast<int>(source.size()); ++i) {
    char current_char = source[i];
    int pos = 0;
    auto it = alphabet.begin();
    while (*it != current_char) {
      ++pos;
      ++it;
    }
    mtf_[i] = static_cast<char>('a' + pos);
    alphabet.erase(it);
    alphabet.push_front(current_char);
  }
  return mtf_;
}

std::string MTF::DecodeMTF(const std::string_view& mtf) {
  if (!source_.empty()) {
    return source_;
  }
  std::list<char> alphabet = kAlphabet;
  mtf_ = mtf;
  source_.resize(mtf.size());
  for (int i = 0; i < static_cast<int>(mtf.size()); ++i) {
    char mtf_char = mtf[i];
    int pos = 0;
    auto it = alphabet.begin();
    while (pos != mtf_char - 'a') {
      ++pos;
      ++it;
    }
    source_[i] = *it;
    alphabet.erase(it);
    alphabet.push_front(source_[i]);
  }
  return source_;
}

class RLE {
 public:
  RLE() = default;
  std::string GetRLE(const std::string_view& source);
  std::string DecodeRLE(const std::string_view& rle);

 private:
  std::string source_;
  std::string rle_;
};
std::string RLE::GetRLE(const std::string_view& source) {
  if (!rle_.empty()) {
    return rle_;
  }
  for (int i = 0; i < static_cast<int>(source.size()); ++i) {
    rle_ += source[i];
    int cnt = 1;
    for (auto ch = source[i];
         i + 1 < static_cast<int>(source.size()) && ch == source[i + 1]; ++i) {
      ++cnt;
    }
    rle_ += std::to_string(cnt);
  }
  return rle_;
}
std::string RLE::DecodeRLE(const std::string_view& rle) {
  if (!source_.empty()) {
    return source_;
  }
  for (int i = 0; i < static_cast<int>(rle.size()); ++i) {
    auto ch = rle[i];
    std::string cnt;
    while (i + 1 < static_cast<int>(rle.size()) &&
           (std::isdigit(rle[i + 1]) != 0)) {
      cnt.push_back(rle[++i]);
    }
    auto sz = std::stoi(cnt);
    source_.resize(source_.size() + sz, ch);
  }
  return source_;
}

class Archiever {
 public:
  Archiever() = default;
  void Start();

 private:
  BWT bwt_obj_;
  MTF mtf_obj_;
  RLE rle_obj_;
};

void Archiever::Start() {
  int flag;
  std::cin >> flag;

  std::string str;
  std::cin >> str;

  if (flag == 1) {
    std::vector<int> suffix_array = BuildSuffixArray(str);
    bwt_obj_.GetBWT(str, suffix_array);
    mtf_obj_.GetMTF(bwt_obj_.GetBWT(str, suffix_array));
    rle_obj_.GetRLE(mtf_obj_.GetMTF(bwt_obj_.GetBWT(str, suffix_array)));
    std::cout << rle_obj_.GetRLE(
            mtf_obj_.GetMTF(bwt_obj_.GetBWT(str, suffix_array)))
              << '\n';
  } else {
    int bwt_pos;
    std::cin >> bwt_pos;
    rle_obj_.DecodeRLE(str);
    mtf_obj_.DecodeMTF(rle_obj_.DecodeRLE(str));
    bwt_obj_.ReverseBWT(mtf_obj_.DecodeMTF(rle_obj_.DecodeRLE(str)), bwt_pos);
    std::cout << bwt_obj_.ReverseBWT(
            mtf_obj_.DecodeMTF(rle_obj_.DecodeRLE(str)), bwt_pos)
              << '\n';
  }
}

int main() {
  Archiever archiever;
  archiever.Start();

  return 0;
}
