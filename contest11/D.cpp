#include <algorithm>
#include <iostream>
#include <vector>

const int kSize = 1048576;

class SuffixArray {
 private:
  std::vector<int> suffix_array_;
  std::string ss_;
  std::vector<int> BuildSuffixArray();
  static void StableSort(const std::vector<int>& arr, std::vector<int>& cnt);
  static std::vector<int> StringToVector(const std::string_view& ss);
  std::vector<std::vector<int>> Prepare();

 public:
  SuffixArray(const std::string_view& ss) : ss_(ss) {
    suffix_array_ = BuildSuffixArray();
  }
  std::vector<int> GetSuffixArray() { return suffix_array_; }
};

void SuffixArray::StableSort(const std::vector<int>& arr,
                             std::vector<int>& cnt) {
  for (int i = 0; i < kSize; i++) {
    cnt[i] = 0;
  }
  int sz = static_cast<int>(arr.size());
  for (int i = 0; i < sz; i++) {
    ++cnt[arr[i]];
  }
  for (int i = 1; i < kSize; i++) {
    cnt[i] += cnt[i - 1];
  }
}

std::vector<int> SuffixArray::StringToVector(const std::string_view& ss) {
  std::vector<int> arr(ss.size());
  for (size_t i = 0; i < ss.size(); i++) {
    arr[i] = ss[i];
  }
  return arr;
}

std::vector<std::vector<int>> SuffixArray::Prepare() {
  int sz = static_cast<int>(ss_.size());
  std::vector<int> classes(sz, -1);  // их не больше sz штук
  std::vector<int> cnt(kSize + sz);
  StableSort(StringToVector(ss_), cnt);
  std::vector<int> pp(sz);  // перестановки
  for (int i = sz - 1; i >= 0; i--) {
    pp[--cnt[ss_[i]]] = i;
  }
  classes[pp[0]] = 0;
  for (int i = 1; i < sz; i++) {
    classes[pp[i]] = classes[pp[i - 1]];
    if (ss_[pp[i]] != ss_[pp[i - 1]]) {
      ++classes[pp[i]];
    }
  }
  return {classes, cnt, pp};
}

std::vector<int> SuffixArray::BuildSuffixArray() {
  int sz = static_cast<int>(ss_.size());
  auto vectors = Prepare();
  std::vector<int>& classes = vectors[0];
  std::vector<int>& cnt = vectors[1];
  std::vector<int>& pp = vectors[2];
  // теперь отсортируем подстроки длинны kk+1 получается сортировка пар
  // создаём массив пар (classes[i], classes[i+2^kk]) сортируем по второму потом
  // стабильно по первому получаем новый pp и classes
  int kk = 0;
  while (sz - (1 << kk) > 0) {
    std::vector<int> new_pp(sz);
    std::vector<int> new_classes(sz);
    new_pp[0] = (sz + pp[0] - (1 << kk)) % sz;
    for (int i = 1; i < sz; i++) {
      new_pp[i] = (sz + pp[i] - (1 << kk)) % sz;
    }
    StableSort(classes, cnt);
    for (int i = sz - 1; i >= 0; i--) {
      pp[--cnt[classes[new_pp[i]]]] = new_pp[i];
    }  // pp - искомая сортировка для 2^(kk+1)
    new_classes[pp[0]] = 0;
    for (int i = 1; i < sz; i++) {
      new_classes[pp[i]] = new_classes[pp[i - 1]];
      if (classes[pp[i]] != classes[pp[i - 1]] ||
          classes[(pp[i] + (1 << kk)) % sz] !=
              classes[(pp[i - 1] + (1 << kk)) % sz]) {
        ++new_classes[pp[i]];
      }
    }
    classes = new_classes;
    kk++;
  }
  return pp;
}

int main() {
  std::string ss;
  std::cin >> ss;
  SuffixArray suffarr(ss);
  std::vector<int> suffix_array = suffarr.GetSuffixArray();
  std::string ans;
  for (size_t i = 0; i < ss.size(); i++) {
    ans += ss[(suffix_array[i] + ss.size() - 1) % ss.size()];
  }
  std::cout << ans << std::endl;
  return 0;
}
